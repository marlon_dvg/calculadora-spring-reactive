package com.ias.calculator.repository;

import com.ias.calculator.model.Service;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface ServiceRepository extends ReactiveCrudRepository<Service, Integer> {

    @Query("SELECT * FROM service WHERE technician_id = @technicianId")
    Flux<Service> findByTechnicianId(Integer technicianId);

    @Query("SELECT * FROM service WHERE service_name LIKE @service_name")
    Flux<Service> findByServiceName(String serviceName);
}
