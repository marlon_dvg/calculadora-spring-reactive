package com.ias.calculator.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table("service")
public class Service {
    @Id
    @Column("id")
    private Integer id;

    @Column("technician_id")
    private Integer technicianId;

    @Column("service_name")
    private String serviceName;

    @Column("initial_date")
    private LocalDateTime initialDate;

    @Column("final_date")
    private LocalDateTime finalDate;
}

