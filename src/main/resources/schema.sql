use calculator;

IF OBJECT_ID(N'dbo.service', N'U') IS NULL
BEGIN
    CREATE TABLE service (
        id BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
        technician_id BIGINT NOT NULL,
        service_name VARCHAR (50) NOT NULL,
        initial_date DATETIME NOT NULL,
        final_date DATETIME NOT NULL
    )
END;